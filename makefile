CPPFILES = $(shell find src -type f -name "*.cpp")
HPPFILES = $(shell find src -type f -name "*.hpp")
.PHONY: all,debug,test,clean

all: client server
	
client: client.cpp $(CPPFILES) $(HPPFILES)
	g++ -std=c++11 client.cpp $(CPPFILES) -fmax-errors=3 -o client

server: server.cpp $(CPPFILES) $(HPPFILES)
	g++ -std=c++11 server.cpp $(CPPFILES) -fmax-errors=3 -o server


debug: $(CPPFILES) $(HPPFILES)
	g++ -std=c++11 $(CPPFILES) -DDEBUG -o debug && ./debug
test: $(CPPFILES) $(HPPFILES)
	g++ -std=c++11 $(CPPFILES) -DTEST -DDEBUG -o test && ./test
clean:
	rm -f debug test net client server
