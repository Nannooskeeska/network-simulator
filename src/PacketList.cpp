#include "PacketList.hpp"

#include "Packet.hpp"
#include <string>
#include <stdexcept>

PacketList::PacketList(int size)
	:size(size)
{
	// allocate space for this many packet pointers:
	packets = new Packet*[size];
	
	// fill it with NULLs so we can tell which packets are valid:
	for(int i = 0; i < size; ++i){
		packets[i] = NULL;
	}
}
PacketList::PacketList(std::string filename)
{
	// TODO read the file and break it up into packets
	// TODO set size
}
PacketList::~PacketList()
{
	// call delete on each packet, then on the array itself
	for(int i = 0; i < size; ++i){
		delete packets[i];
	}
	delete[] packets;
}
Packet* PacketList::get_packet(int packet_id) const
{
	validate_packet_id(packet_id);
	return packets[packet_id];
}
void PacketList::add_packet(Packet* p)
{
	int packet_id = p->get_packet_id();
	validate_packet_id(packet_id);
	
	// copy the packet to its spot in the array:
	packets[packet_id] = p;
}
void PacketList::validate_packet_id(int packet_id) const
{
	// bounds check:
	if(packet_id < 0) throw std::runtime_error(std::string("packet_id cannot be negative in PacketList::get_packet() but you passed it ") + std::to_string(packet_id));
	if(packet_id >= size) throw std::runtime_error(std::string("cannot get packet_id ") + std::to_string(packet_id) + " from PacketList with size " + std::to_string(size));
}
