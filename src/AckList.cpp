#include "AckList.hpp"

#include <vector>
#include <algorithm>

AckList::AckList(int num_packets)
{
	acks = std::vector<bool>();
	acks.reserve(num_packets);
	std::fill(acks.begin(), acks.end(), false);
}

void AckList::add_ack(int packet_num)
{
	acks[packet_num] = true;
}

bool AckList::has_ack(int packet_num)
{
	return acks[packet_num];
}
