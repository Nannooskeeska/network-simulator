#pragma once

#include "SocketClient.hpp"

class Sender{
	private:
		SocketClient socket;
	public:
		Sender(std::string hostname, int port);
		void send(std::string filename);
};
