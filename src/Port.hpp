#pragma once

class Port{
	private:
		int port;
	public:
		Port();
		Port(int port);
		void set(int port);
		int get();
		operator int(){return port;}
};
