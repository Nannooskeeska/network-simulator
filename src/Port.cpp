#include "Port.hpp"

#include <iostream>

Port::Port(){
	std::cout << "port: ";
	std::cin >> port;
}
Port::Port(int port)
	:port(port)
{}

void Port::set(int port){
	this->port = port;
}
int Port::get(){
	return port;
}
