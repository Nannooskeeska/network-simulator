#pragma once

#include <string>
#include <vector>

class Settings
{
	private:
		Settings();
		Settings(Settings const&)=delete;
		void operator=(Settings const&)=delete;
		char protocol; // 's' = stop & wait, 'g' = go back n, 'r' = stop and repeat
		int packet_size; // in bytes
		int timeout; // in ms. -1 means dynamically calculated.
		int window_size;
		int sequence_bits; // # of bits per sequence number
		char error_type; // 'n' = no errors, 'r' = random errors, 'u' = user specified errors
		std::vector<int> drop_packets; // which packets to drop (only counts when errors == 'u')
		std::vector<int> drop_acks; // which acks to drop (only counts when errors == 'u')
		
		void read();
		void read_protocol();
		void read_packet_size();
		void read_timeout();
		void read_window_size();
		void read_sequence_bits();
		void read_error_type();
		void read_drop_packets();
		void read_drop_acks();
	public:
		static Settings& get_instance(){
			static Settings instance;
			return instance;
		}
		inline char get_protocol() const{return protocol;}
		inline int get_packet_size() const{return packet_size;}
		inline int get_timeout() const{return timeout;}
		inline int get_window_size() const{return window_size;}
		inline int get_sequence_bits() const{return sequence_bits;}
		inline char get_error_type() const{return error_type;}
		inline std::vector<int> get_drop_packets() const{return drop_packets;}
		inline std::vector<int> get_drop_acks() const{return drop_acks;}
		
		inline void set_protocol(char protocol){this->protocol = protocol;}
		inline void set_packet_size(int packet_size){this->packet_size = packet_size;}
		inline void set_timeout(int timeout){this->timeout = timeout;}
		inline void set_window_size(int window_size){this->window_size = window_size;}
		inline void set_sequence_bits(int sequence_bits){this->sequence_bits = sequence_bits;}
		inline void set_error_type(char error_type){this->error_type = error_type;}
		inline void set_drop_packets(std::vector<int> drop_packets){this->drop_packets = drop_packets;}
		inline void set_drop_acks(std::vector<int> drop_acks){this->drop_acks = drop_acks;}
};
