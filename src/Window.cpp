#include "Window.hpp"

#include "Settings.hpp"
#include <stdexcept>

Window* Window::instance = NULL;

Window::Window()
{
	// remember this instance so we can access it from anywhere else
	if(instance == NULL){
		instance = this;
	}
}
Window* Window::get_instance(){
	if(instance == NULL){
		throw std::runtime_error("error: called Window::get_instance() before there was an instance.");
	}
	return instance;
}
void Window::shift() 
{

}

int Window::get_right_wall()
{
	return get_left_wall() + get_size();
}

int Window::get_size()
{
	return Settings::get_instance().get_window_size();
}
int Window::get_packet_id(int sequence_num)
{
	int left = get_left_wall() - (get_left_wall() % get_size());
	return left + sequence_num;
}
