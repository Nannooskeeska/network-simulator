#include "FileWriter.hpp"

#include <string>
#include <queue>
#include <iostream>
#include <set>
#include "Packet.hpp"
#include "Settings.hpp"

FileWriter::FileWriter(std::string filename)
{
	file.open(filename, std::ofstream::binary);
	next_packet_id = 0;
}
FileWriter::~FileWriter()
{
	file.close();
	// if we have any leftover packets in the queue, output a message that says that and delete them
	while(!queue.empty()){
		Packet* p = queue.top();
		pids.erase(p->get_packet_id());
		std::cerr << "FileWriter had packet_id " << p->get_packet_id() << " in its queue after closing the file." << std::endl;
		delete p;
	}
}

void FileWriter::write(Packet* packet)
{
	// if we're already past this packet_id, don't do anything:
	if(packet->get_packet_id() < next_packet_id){
		#ifdef DEBUG
		std::cout << "FileWriter::write() was given packet_id " << packet->get_packet_id() << " again after having already written it. throwing it away." << std::endl;
		#endif
		return;
	}
	
	// if this is the packet we need, write it to the file:
	if(packet->get_packet_id() == next_packet_id){
		write_packet(packet);
		return;
	}
	
	// in this context, the packet will be be written later, queue it up if we haven't gotten a packet with that id yet
	if(pids.find(packet->get_packet_id()) == pids.end()){ // if we don't already have that packet
		#ifdef DEBUG
		std::cout << "FileWriter::write() was given packet_id " << packet->get_packet_id() << " but was expecting " << next_packet_id << ". queuing it for later." << std::endl;
		#endif
		queue.push(packet);
		pids.insert(packet->get_packet_id());
		
		// display a potential error message if there are more queued packets than the window size
		if(pids.size() > Settings::get_instance().get_window_size()){
			std::cerr << "Potential Error: There are now " << pids.size() << " packets queued in the FileWriter, but the window size is only " << Settings::get_instance().get_window_size();
		}
	}
	#ifdef DEBUG
	else{
		std::cout << "FileWriter::write() was given future packet_id " << packet->get_packet_id() << " again. throwing the duplicate away." << std::endl;
	}
	#endif
}
void FileWriter::write_packet(Packet* packet)
{
	#ifdef DEBUG
	std::cout << "writing packet_id " << packet->get_packet_id() << " to the file (" << packet->get_size() << " bytes)" << std::endl;
	#endif
	// write this packet's data to the file:
	file.write(packet->get_bytes(), packet->get_size());
	delete packet;
	
	// now we're expecting the next packet:
	next_packet_id++;
	
	// check to see if we already have the next packet:
	if(!queue.empty()){
		Packet* next_packet = queue.top();
		if(next_packet->get_packet_id() == next_packet_id){
			#ifdef DEBUG
			std::cout << "FileWriter already has packet_id " << next_packet_id << " so I'll right that write away!" << std::endl;
			#endif
			// remove it from the queue:
			queue.pop();
			pids.erase(next_packet->get_packet_id());
			// write this next packet:
			write_packet(next_packet);
		}
	}
}
