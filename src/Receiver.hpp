#pragma once

#include "SocketServer.hpp"

class Receiver{
	private:
		int port;
		SocketServer socket;
	public:
		Receiver();
		void receive();
};
