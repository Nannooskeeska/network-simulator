#pragma once

#include <string>
#include <fstream>
class Packet;

class FileReader
{
	private:
		int next_packet_id;
		std::ifstream file;
		unsigned long int filesize;
		Packet* read_next_packet();
	public:
		FileReader(std::string filename);
		~FileReader();
		Packet* get_packet(int packet_id);
		Packet* get_next();
		inline int get_next_packet_id(){return next_packet_id;}
		int get_packet_count() const;
		bool has_more_packets() const;
};