#include "Receiver.hpp"
#include "Settings.hpp"
#include "SocketServer.hpp"
#include "Port.hpp"
#include <iostream>

Receiver::Receiver()
	:port(Port())
	,socket(SocketServer(port))
{}
void Receiver::receive()
{
	socket.connect();
	socket.listen();
	std::cout << "server listening on port " << port << std::endl;
	while(socket.is_connected()){
		// send settings to the new client
		socket << Settings::get_instance();
		
		// receive a file
		std::cerr << "TODO Receiver::receive()" << std::endl;
		// save the file
		// validate the file with md5 hash
		socket.disconnect();
	}
}
