#pragma once

#include <string>
#include <fstream>
#include <queue>
#include <set>
class Packet;

class FileWriter
{
	private:
		std::ofstream file;
		std::priority_queue<Packet*> queue;
		std::set<int> pids; // remembers all packet_ids of all queued packets
		int next_packet_id;
		void write_packet(Packet* packet);
	public:
		FileWriter(std::string filename);
		~FileWriter();
		
		// this function will make sure this packet is written eventually.
		// this takes ownership of the packet object, so don't worry about deleting it elsewhere.
		void write(Packet* packet);
};