#include "SocketClient.hpp"

#include <stdio.h>
#include <errno.h>
#include <stdexcept>
#include <cstdlib>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

SocketClient::SocketClient(const std::string& hostname, int port)
{
	struct sockaddr_in serv_addr;
	
	fd = socket(AF_INET, SOCK_STREAM, 0);
	if(fd < 0){
		throw std::runtime_error(std::string("error opening socket: ") + strerror(errno));
	}
	
	// find the server by hostname:
	struct hostent *server = gethostbyname(hostname.c_str());
	if(server == NULL){
		throw std::runtime_error(std::string("error: host '") + hostname + "' could not be found.");
	}
	
	// make the sock_addr_in struct:
	bzero((char*)&serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char*)&serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(port);
	
	// try to actually connect to the server:
	if(connect(fd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0){
		throw std::runtime_error("error connecting");
	}
}
