#include "DSock.hpp"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdexcept>
#include <string>

DSock::DSock()
{
	fd = socket(AF_INET6, SOCK_DGRAM, 0);
	if(fd < 0){
		throw std::runtime_error("error while opening dgram socket");
	}
}
