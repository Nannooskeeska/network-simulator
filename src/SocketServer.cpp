#include "SocketServer.hpp"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdexcept>
#include <errno.h>
#include <cstdlib>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

SocketServer::SocketServer(int port)
{
	fd = -1;
	struct sockaddr_in serv_addr;
	
	// try to open the socket:
	server_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(server_fd < 0){
		throw std::runtime_error(std::string("error opening socket: ") + strerror(errno));
	}
	
	// get info about the server:
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(port);
	
	// assign a name to this socket:
	int n = bind(server_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
	if(n < 0){
		throw std::runtime_error(std::string("error: unable to bind: ") + strerror(errno));
	}
}
SocketServer::~SocketServer()
{
	disconnect();
	close(server_fd);
}

bool SocketServer::is_connected()
{
	return fd >= 0;
}

void SocketServer::listen()
{
	// start listening for connections:
	int n = ::listen(server_fd, MAX_CONNECTIONS);
	std::cout << "listen: " << n << std::endl;
	if(n < 0){
		std::cerr << "error while listening: " << strerror(errno) << std::endl;
	}
}

void SocketServer::connect()
{
	// disconnect just in case whoever called connect last didn't disconnect
	disconnect();
	
	// get client address information:
	struct sockaddr_in cli_addr;
	socklen_t clilen = sizeof(cli_addr);
	
	// accept the connection:
	fd = accept(server_fd, (struct sockaddr*)&cli_addr, &clilen);
	if(fd < 0){
		std::cerr << "error on accept: " << strerror(errno) << std::endl;
		std::cerr << "server_fd: " << server_fd << std::endl;
		throw std::runtime_error("error in SocketServer::connect()");
	}
}

void SocketServer::disconnect()
{
	if(fd >= 0){
		close(fd);
		fd = -1;
	}
}
