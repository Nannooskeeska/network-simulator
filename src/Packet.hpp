#pragma once

class Packet
{
	private:
		int packet_id;
		char* bytes;
		int size; // number of bytes in bytes
	public:
		Packet(int id, char* bytes, int size);
		~Packet();
		inline char* get_bytes() const{return bytes;}
		inline int get_packet_id() const{return packet_id;}
		inline int get_size() const{return size;}
		int get_sequence_num() const;
};