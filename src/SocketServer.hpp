#pragma once

#include "Socket.hpp"

class SocketServer : public Socket
{
	private:
		int server_fd;
	public:
		static const int MAX_CONNECTIONS = 5;
		
		SocketServer(int port);
		~SocketServer();
		
		void listen();	
		void connect();
		bool is_connected();
		void disconnect();
};
