#include "Socket.hpp"

#include <iostream>
#include <stdio.h>
#include <errno.h>
#include <stdexcept>
#include <cstdlib>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <vector>
#include "Window.hpp"
#include "Packet.hpp"
#include "Settings.hpp"

Socket::~Socket()
{
	close(fd);
}
int Socket::read(void *buffer, int nbytes)
{
	int flags = 0;
	int nbytesreceived = ::recv(fd, buffer, nbytes, flags);
	if(nbytesreceived == -1){
		std::cerr << "error reading from socket: " << strerror(errno) << std::endl;
	}
	return nbytesreceived;
}
int Socket::write(const void *buffer, int nbytes)
{
	int flags = 0;
	int nbytessent = ::send(fd, buffer, nbytes, flags);
	if(nbytessent == -1){
		std::cerr << "error writing to socket: " << strerror(errno) << std::endl;
	}
	return nbytessent;
}


void Socket::read_buffer(char* buffer, int* size)
{
	// read the size:
	if(read(size, sizeof(int)) == -1){
		throw std::runtime_error("failed to read buffer size in Socket::read_buffer");
	}
	
	// create the buffer:
	buffer = new char[*size];
	
	// read the buffer:
	unsigned int offset = 0;
	while(offset < *size){
		// figure out where this packet starts in the buffer:
		char *addr = buffer + offset;
		int bytes_left = *size - offset;
		int bytes_read = read(addr, bytes_left);
		if(bytes_read < 0) throw std::runtime_error("failed to read buffer data in Socket::read_buffer()");
		offset += bytes_read;
	}
}
void Socket::write_buffer(char* buffer, int size)
{
	// write the size:
	write(&size, sizeof(int));
	
	// write all the data:
	int offset = 0;
	while(offset < size){
		char *addr = buffer + offset;
		int bytes_left = size - offset;
		int bytes_written = write(addr, bytes_left);
		if(bytes_written) throw std::runtime_error("failed to write buffer data in Socket::write_buffer()");
		offset += bytes_written;
	}
}

void Socket::operator<<(int& i){
	write(&i, sizeof(int));
}
void Socket::operator>>(int& i)
{
	read(&i, sizeof(int));
}
void Socket::operator<<(unsigned int& i)
{
	write(&i, sizeof(int));
}
void Socket::operator>>(unsigned int& i)
{
	read(&i, sizeof(int));
}
void Socket::operator<<(char& c){
	write(&c, sizeof(char));
}
void Socket::operator>>(char& c){
	read(&c, sizeof(char));
}

void Socket::operator<<(const std::string& message)
{
	int nbytes = message.length() + 1; // +1 for NULL at the end
	write(message.c_str(), nbytes);
}
void Socket::operator>>(std::string& output)
{
	int buf_size = 1024;
	char buffer[buf_size];
	if(read(&buffer, buf_size - 1)){
		output = std::string(buffer);
	}
}

void Socket::operator<<(const Packet& packet)
{
	int sequence_num = packet.get_sequence_num();
	write(&sequence_num, sizeof(int));
	write_buffer(packet.get_bytes(), packet.get_size());
}
void Socket::operator>>(Packet& output)
{
	// first, read sequence number:
	int sequence_num;
	read(&sequence_num, sizeof(int));
	
	// read the data:
	char* buffer;
	int size;
	read_buffer(buffer, &size);
	
	// create the packet:
	int packet_id = Window::get_instance()->get_packet_id(sequence_num);
	Packet* p = new Packet(packet_id, buffer, size);
	output = *p;
}
void Socket::operator<<(const Settings& settings)
{
	// send settings
	int i= settings.get_protocol();
	this->operator<<(i);
	i = settings.get_packet_size();
	this->operator<<(i);
	i = settings.get_timeout();
	this->operator<<(i);
	i = settings.get_window_size();
	this->operator<<(i);
	i = settings.get_sequence_bits();
	this->operator<<(i);
	char c = settings.get_error_type();
	this->operator<<(c);
	// drop packets:
	std::vector<int> drop_packets = settings.get_drop_packets();
	int drop_packets_count = drop_packets.size();
	this->operator<<(drop_packets_count);
	for(auto it = drop_packets.begin(); it != drop_packets.end(); ++it){
		this->operator<<(*it);
	}
	// drop acks:
	std::vector<int> drop_acks = settings.get_drop_acks();
	int drop_acks_count = drop_acks.size();
	this->operator<<(drop_acks_count);
	for(auto it = drop_acks.begin(); it != drop_acks.end(); ++it){
		this->operator<<(*it);
	}
}
void Socket::operator>>(Settings& settings)
{
	std::cout << "getting settings from server.." << std::endl;
	// receive settings
	int i;
	char c;
	*this >> i;
	settings.set_protocol(i);
	*this >> i;
	settings.set_packet_size(i);
	*this >> i;
	settings.set_timeout(i);
	*this >> i;
	settings.set_window_size(i);
	*this >> i;
	settings.set_sequence_bits(i);
	*this >> c;
	settings.set_error_type(c);
	
	// drop packets:
	int drop_packets_count;
	*this >> drop_packets_count;
	std::vector<int> drop_packets;
	drop_packets.reserve(drop_packets_count);
	for(int j = 0; j < drop_packets_count; ++j){
		*this >> i;
		drop_packets[j] = i;
	}

	// drop acks:
	int drop_acks_count;
	*this >> drop_acks_count;
	std::vector<int> drop_acks;
	drop_acks.reserve(drop_acks_count);
	for(int j = 0; j < drop_acks_count; ++j){
		*this >> i;
		drop_acks[j] = i;
	}
	std::cout << "all set up!" << std::endl;
}
