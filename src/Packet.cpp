#include "Packet.hpp"

#include "Settings.hpp"

Packet::Packet(int id, char* bytes, int size)
	:packet_id(id)
	,bytes(bytes)
	,size(size)
{}
Packet::~Packet()
{
	// once packets get their bytes, they have ownership of them, so the packet must delete
	delete[] bytes;
}

int Packet::get_sequence_num() const
{
	return packet_id % Settings::get_instance().get_window_size();
}
