#pragma once

#include <string>
class Packet;

class PacketList
{
	private:
		Packet** packets;
		int size;
		void validate_packet_id(int packet_id) const;
	public:
		PacketList(int size);
		PacketList(std::string filename);
		~PacketList();
		Packet* get_packet(int packet_id) const;
		void add_packet(Packet* p);
		bool has_packet(int packet_id);
		inline int get_size() const{return size;}
};