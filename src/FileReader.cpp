#include "FileReader.hpp"

#include <string>
#include <fstream>
#include "Packet.hpp"
#include "Settings.hpp"
#include <iostream>

FileReader::FileReader(std::string filename)
{
	// open the file:
	file.open(filename, std::ifstream::ate | std::ifstream::binary);
	
	// get the size of the file:
	filesize = file.tellg();
	
	// set the streampos to the beginning to match next_packet_id:
	file.seekg(file.beg);
	next_packet_id = 0;
}
FileReader::~FileReader()
{
	file.close();
}
Packet* FileReader::read_next_packet()
{
	// read in the data and allocate a new Packet for that
	int size = Settings::get_instance().get_packet_size();
	
	// if this is the last packet, change the size to be the remainder of the file
	if(next_packet_id == (get_packet_count()-1)){
		size = filesize % size;
		if(size == 0){ // the file is exactly divisible into packets
			size = Settings::get_instance().get_packet_size();
		}
	}
	
	#ifdef DEBUG
	std::cout << "creating packet_id " << next_packet_id << " with size " << size << " (out of " << filesize << " total)" << std::endl;
	#endif
	
	// read the data from the file:
	char* buffer = new char[size];
	file.read(buffer, size);
	return new Packet(next_packet_id, buffer, size);
}
Packet* FileReader::get_packet(int packet_id)
{
	// calculate which byte to start reading from:
	int size = Settings::get_instance().get_packet_size();
	int offset = packet_id * size;
	
	// set the input stream's position to read this packet's data
	file.seekg(offset, file.beg);
	
	// read the packet:
	Packet* p = read_next_packet();
	
	// update the stream pointer to match the next_packet_id
	file.seekg(next_packet_id * size);
	
	return p;
}

Packet* FileReader::get_next()
{
	Packet* p = read_next_packet();
	next_packet_id++;
	return p;
}

int FileReader::get_packet_count() const
{
	return ((filesize-1) / Settings::get_instance().get_packet_size()) + 1;
}
bool FileReader::has_more_packets() const
{
	return next_packet_id < get_packet_count();
}
