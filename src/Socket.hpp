#pragma once

#include <string>
class Packet;
class Settings;

class Socket
{
	protected:
		int fd;
		int read(void *buffer, int nbytes);
		int write(const void *buffer, int nbytes);
		void read_buffer(char* buffer, int* size);
		void write_buffer(char* buffer, int size);
	public:
		~Socket();
		
		void operator<<(int& i);
		void operator>>(int& i);
		void operator<<(unsigned int& i);
		void operator>>(unsigned int& i);
		void operator<<(char& i);
		void operator>>(char& i);
		
		void operator<<(const std::string& message);
		void operator>>(std::string& output);
		
		void operator<<(const Settings& settings);
		void operator>>(Settings& settings);
		
		void operator<<(const Packet& packet);
		void operator>>(Packet& output);
};
