#pragma once

#include <vector>

class AckList 
{
	private:
		std::vector<bool> acks;
	public:
		AckList(int num_packets);
		void add_ack(int packet_id);
		bool has_ack(int packet_id);
};