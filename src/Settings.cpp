#include "Settings.hpp"

#include <string>
#include <vector>
#include <iostream>
#include <sstream>

Settings::Settings()
{
	read_protocol();
	read_packet_size();
	read_timeout();
	read_window_size();
	read_sequence_bits();
	read_error_type();
	read_drop_packets();
	read_drop_acks();
}
void Settings::read_protocol()
{
	std::cout << "protocol: [s]top & wait, [g]o back n, stop and [r]epeat ";
	std::cin >> protocol;
}
void Settings::read_packet_size()
{
	std::cout << "packet size (in bytes): ";
	std::cin >> packet_size;
}
void Settings::read_timeout()
{
	std::cout << "timeout (in ms): ";
	std::cin >> timeout;
}
void Settings::read_window_size()
{
	std::cout << "window size: ";
	std::cin >> window_size;
}
void Settings::read_sequence_bits()
{
	std::cout << "sequence bits: ";
	std::cin >> sequence_bits;
}
void Settings::read_error_type()
{
	std::cout << "error type: [n]o errors, [r]andom errors, [u]ser specified errors: ";
	std::cin >> error_type;
}
void Settings::read_drop_packets()
{
	if(error_type == 'u'){
		std::cout << "enter all packet #s to drop. enter a blank line to finish.";
		std::string line;
		while(std::getline(std::cin, line)){
			if(line == "") break;
			int packet;
			std::istringstream(line) >> packet;
			drop_packets.push_back(packet);
		}
	}
}
void Settings::read_drop_acks()
{
	if(error_type == 'u'){
		std::cout << "enter all ack #s to drop. enter a blank line to finish.";
		std::string line;
		while(std::getline(std::cin, line)){
			if(line == "") break;
			int ack;
			std::istringstream(line) >> ack;
			drop_acks.push_back(ack);
		}
	}
}
