#pragma once

class Window
{
	private:
		//todo: should we also keep track of the size here?
		int left_wall;
		static Window* instance;
	public:
		Window();
		static Window* get_instance();
		void shift();
		int get_size(); //todo: make this inline????
		inline int get_left_wall(){return left_wall;}
		int get_right_wall();
		int get_packet_id(int sequence_num);
};