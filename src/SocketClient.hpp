#pragma once

#include "Socket.hpp"

class SocketClient : public Socket
{
	public:
		SocketClient(const std::string& hostname, int port);
};
