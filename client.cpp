#include "src/Sender.hpp"

#include <string>
#include <iostream>
#include "src/Port.hpp"

int main()
{
	std::cout << "host: ";
	std::string hostname;
	std::cin >> hostname;
	
	int port = Port();
	Sender s(hostname, port);
	
	std::cout << "file: ";
	std::string filename;
	std::cin >> filename;

	s.send(filename);
	
	return 0;
}
